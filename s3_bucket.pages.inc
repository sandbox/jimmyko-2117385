<?php

/**
 * Bucket view callback.
 */
function s3_bucket_view($bucket) {
  return entity_view('s3_bucket', array(entity_id('s3_bucket', $bucket) => $bucket), 'full');
}
