<?php

/**
 * @file
 * The class for S3 Bucket entity.
 */

/**
 * The controller class for instantiating S3 bucket entity.
 */
class S3Bucket extends Entity {

  public $name;

  /**
   * Override this in order to specify fixed entity type because we
   * only have one type for S3 bucket entity.
   */
  public function __construct(array $values = array(), $entityType = 's3_bucket') {
    $info = entity_get_info($entityType);

    if (empty($entityType)) {
      throw new Exception('Cannot create an instance of S3 Bucket with out a specified bucket type.');
    }
    $this->entityType = $entityType;
    $this->{$info['entity keys']['bundle']} = $entityType;
    $this->setUp();
    // Set initial values.
    foreach ($values as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   * Override this to implement a custom default label.
   */
  protected function defaultLabel() {
    return t('Bucket @name', array('@name' => $this->getTranslation($this->entityInfo['entity keys']['name'])));
  }

  /**
   * Override this to implement a custom default URI.
   */
  protected function defaultUri() {
    return array('path' => 's3-bucket/' . $this->internalIdentifier());
  }
}
