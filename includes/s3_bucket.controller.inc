<?php

/**
 * @file
 * The controller classes for S3 Bucket entity.
 */

/**
 * Extends the default CRUD behaviors.
 */
class S3BucketAPIController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'bid' => NULL,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => '',
    );

    return parent::create($values);
  }

  public function save($bucket, DatabaseTransaction $transaction = NULL) {
    $bucket->changed = REQUEST_TIME;
    try {
      return parent::save($bucket, $transaction);
    }
    catch (Exception $e) {
      throw $e;
    }
  }
}

/**
 * Provide extra metadata to entity.
 */
class S3BucketMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $arr = parent::entityPropertyInfo();

    $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t("The date the node was posted."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer nodes',
      'schema field' => 'created',
    );
    $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'schema field' => 'changed',
      'description' => t("The date the node was most recently updated."),
    );
    $properties['uid'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("The author of the task."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer example_task entities',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    $arr[$this->type]['properties'] += $properties;
    return $arr;
  }
}
