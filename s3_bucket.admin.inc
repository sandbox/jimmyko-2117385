<?php

function s3_bucket_add_form($form, &$form_state) {
  drupal_set_title(t('Create a bucket'));
  $bucket = entity_get_controller('s3_bucket')->create();
  return s3_bucket_form($form, $form_state, $bucket);
}

/**
 * A form for editing, adding and clonging.
 *
 * @see entity_crud_hook_entity_info().
 */
function s3_bucket_form($form, &$form_state, $bucket) {
  $form_state['s3_bucket'] = $bucket;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The bucket name in Amazon S3.'),
    '#default_value' => isset($bucket->name) ? $bucket->name : '',
    '#required' => TRUE,
    '#element_validate' => array('s3_bucket_name_validate'),
  );

  if (!isset($bucket->uid)) {
    global $user;
    $bucket->uid = $user->uid;
  }
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $bucket->uid,
  );

  field_attach_form('s3_bucket', $bucket, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save S3 bucket'),
    '#submit' => $submit + array('s3_bucket_form_submit'),
  );

  // Show Delete button if we edit task.
  $bucket_id = entity_id('s3_bucket', $bucket);
  if (!empty($bucket_id) && s3_bucket_access('edit', $bucket)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('s3_bucket_form_submit_delete'),
    );
  }

  $form['#validate'][] = 's3_bucket_form_validate';

  return $form;
}

/**
 * Bucket Name validation.
 *
 * A bucket name must comply with the following rules.
 * - at least 3 and no more then 63 characters long.
 * - contains a series of one or more labels. Adjacent labels are separated by a
 *   single period (.). Bucket names can contain lowercase letters, numbers, and
 *   dashes. Each label must start and end with lowercase letter or a
 *   number.
 * - MUST NOT be formatted as an IP address (e.g., 192.168.5.4).
 * Though bucket names in the US Standard region has less restrictive. We would
 * like to make this validation more globally used. So we follow the more
 * restricted rules.
 *
 * @see http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html
 */
function s3_bucket_name_validate($element, &$form_state, $form) {

  $length = strlen($element['#value']);
  if ($length < 3 || $length > 63) {
    form_error($element, t('Bucket name must be at least 3 and no more then 63 characters long.'));
  }

  $labels = explode('.', $element['#value']);
  foreach ($labels as $label) {
    if ( !ctype_lower($label{0})
      && !is_numeric($label{0})
      && !ctype_lower(substr($label, -1))
      && !is_numeric(substr($label, -1))
      && preg_match('/^[0-9a-z]+$/', $label) ) {
      form_error($element, t('Buckets name must contains a series of one or more labels. Adjacent labels are separated by a single period (.). Bucket names can contain lowercase letters, numbers, and dashes. Each label must start and end with lowercase letter or a number.'));
    }
  }

  if (filter_var($element['#value'], FILTER_VALIDATE_IP)) {
    form_error($element, t('Buckets name MUST NOT be formatted as an IP address (e.g., 192.168.5.4).'));
  }
}

/**
 * S3 Bucket form validate handler.
 */
function s3_bucket_form_validate($form, &$form_state) {
  // TODO
}

/**
 * S3 Bucket submit handler.
 */
function s3_bucket_form_submit($form, &$form_state) {
  $bucket = $form_state['s3_bucket'];

  entity_form_submit_build_entity('s3_bucket', $bucket, $form, $form_state);

  // Save bucket
  s3_bucket_save($bucket);
}

/**
 * Field management form of S3 Bucket.
 */
function s3_bucket_edit($bid) {
  $bucket = s3_bucket_load($bid);
  return entity_ui_get_form('s3_bucket', $bucket, 'edit');
}

function s3_bucket_form_submit_delete(&$form, &$form_state) {
  $bucket = $form_state['s3_bucket'];
  $form_state['redirect'] = 's3-bucket/' . $bucket->bid . '/delete';
}

/**
 * Delete confirmation form.
 */
function s3_bucket_delete_confirm_form($form, &$form_state, $bucket) {
  $form_state['s3_bucket'] = $bucket;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['bid'] = array(
    '#type' => 'value',
    '#value' => entity_id('s3_bucket', $bucket),
  );
  $bucket_uri = entity_uri('s3_bucket', $bucket);
  return confirm_form($form,
    t('Are you sure you want to delete bucket %name?', array('%name' => s3_bucket_label($bucket))),
    $bucket_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete confirmation form submit handler.
 */
function s3_bucket_delete_confirm_form_submit($form, &$form_state) {
  $bucket = $form_state['s3_bucket'];
  s3_bucket_delete($bucket);

  $form_state['redirect'] = 'admin/structures/s3-buckets';
}
